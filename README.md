# 工程简介

## 1、组织架构

| 目录               | 简介                                                         |
| ------------------ | ------------------------------------------------------------ |
| hand-train         | 父工程，版本控制                                             |
| hand-common        | 通用模块，如通用结果(Result)、状态码(BizCodeEnum)            |
| hand-config-server | 配置中心，对应仓库为[cloud-config](https://gitee.com/tobing/cloud-config/tree/master/hand-train) |
| hand-eureka-server | 注册中心                                                     |
| hand-gateway       | 网关                                                         |
| hand-member        | 会员服务，配置管理                                           |
| hand-order         | 订单服务，模拟远程调用，熔断                                 |
| hand-auth          | 认证授权服务                                                 |

![hand_train_struct](https://gitee.com/tobing/imagebed/raw/master/hand_train_struct.jpg)

## 2、后端技术

| 技术                 | 说明               |
| -------------------- | ------------------ |
| Eureka               | 注册中心           |
| OpenFeign            | 远程调用           |
| Hystrix              | 服务降级、服务熔断 |
| Spring Cloud Config  | 配置中心           |
| Spring Cloud Gateway | 网关               |
| Ribbon               | 均衡负载           |
| Spring Security      | 权限管理           |

> 本项目基于Spring Boot 2.3.7.RELEASE，Spring Cloud Hoxton.SR9

## 3、功能测试

## 4、额外说明

补充实现

+ 通用[结果集](https://gitee.com/tobing/hand-train/blob/master/hand-common/src/main/java/top/tobing/common/Result.java)与[状态码](https://gitee.com/tobing/hand-train/blob/master/hand-common/src/main/java/top/tobing/common/BziCodeEnum.java)
+ [使用JSR303进行参数校验](https://gitee.com/tobing/hand-train/blob/master/hand-member/src/main/java/top/tobing/controller/HandEmployeeController.java#L46-60)
+ [网关异常处理](https://gitee.com/tobing/hand-train/blob/master/hand-gateway/src/main/java/top/tobing/exception/GlobalExceptionHandler.java)





