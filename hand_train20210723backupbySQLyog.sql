/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.18-log : Database - hand_train
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hand_train` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hand_train`;

/*Table structure for table `hand_departments` */

CREATE TABLE `hand_departments` (
  `department_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `department_code` varchar(30) NOT NULL COMMENT '部门编号',
  `department_name` varchar(80) NOT NULL COMMENT '部门名称',
  `manager_id` bigint(20) DEFAULT NULL COMMENT '部门主管(hand_employee.employee_id)',
  `location_id` bigint(20) DEFAULT NULL COMMENT '部门地址',
  PRIMARY KEY (`department_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=505 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

/*Data for the table `hand_departments` */

insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (101,'D01','华东技术中心',NULL,1002);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (201,'D02','华北技术中心',NULL,NULL);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (301,'D03','华南技术中心',NULL,NULL);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (401,'D04','华中技术中心',NULL,1001);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (501,'D05','财务技术中心',NULL,NULL);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (502,'test','teset',2,2);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (503,'POSTMAN_P','POSTMAN_P',2,3);
insert  into `hand_departments`(`department_id`,`department_code`,`department_name`,`manager_id`,`location_id`) values (504,'UPDATE','技术部更新测试',1,1);

/*Table structure for table `hand_employees` */

CREATE TABLE `hand_employees` (
  `employee_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `employee_num` varchar(30) NOT NULL COMMENT '人员编号',
  `first_name` varchar(20) NOT NULL COMMENT '名',
  `Last_name` varchar(20) DEFAULT NULL COMMENT '姓',
  `sex` varchar(10) DEFAULT NULL COMMENT '性别',
  `phone_num` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `email` varchar(80) DEFAULT NULL COMMENT '电子邮件',
  `hire_date` datetime DEFAULT NULL COMMENT '入职日期',
  `department_id` bigint(20) DEFAULT NULL COMMENT '部门ID(hand_departments.department_id)',
  `status_code` varchar(30) NOT NULL COMMENT '员工状态',
  PRIMARY KEY (`employee_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

/*Data for the table `hand_employees` */

insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (1,'101','昊天','唐','男','15071231777','tangsan@dl.com','2021-01-27 20:36:44',101,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (2,'102','小五','赵','女','15071231888','xxxx@hand-china.com','2021-02-27 20:36:48',101,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (3,'103','强','王','男',NULL,NULL,'2021-03-27 20:36:48',201,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (4,'104','强','李','男',NULL,NULL,'2020-04-27 20:36:48',201,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (5,'105','杰','刘','男',NULL,'22939877@1@qq.com','2021-04-14 20:36:48',201,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (6,'106','炎','萧','男',NULL,'xiaoyan@dp.com','2021-04-25 20:36:48',301,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (7,'107','薰儿','张','女',NULL,NULL,'2021-01-13 20:36:48',301,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (8,'108','彩鳞','刘','女',NULL,NULL,'2021-01-12 20:36:48',301,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (9,'1','liang','shangfu','nan','12412312221','sadfa@hand-china.com','2021-01-01 00:00:00',1,'AXID');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (10,'1022','昊天yy','唐yys','nan',NULL,'tsfaangsan@dl.comx','2021-02-26 00:00:00',102,'ACTsN');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (11,'1234','尚福','梁','男',NULL,'tobing@hand-china.com','2021-01-11 00:00:00',1,'ACID');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (12,'101','昊天','唐','男','1507123177','tangsan@dl.com','2021-01-27 20:36:44',101,'ACTV');
insert  into `hand_employees`(`employee_id`,`employee_num`,`first_name`,`Last_name`,`sex`,`phone_num`,`email`,`hire_date`,`department_id`,`status_code`) values (13,'101','昊天','唐','男','150712317','tangsan@dl.com','2021-01-27 20:36:44',101,'ACTV');

/*Table structure for table `hand_jobs` */

CREATE TABLE `hand_jobs` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `job_code` varchar(30) NOT NULL COMMENT '职位编号',
  `job_name` varchar(80) NOT NULL COMMENT '职位名称',
  `primary_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否主岗(1:是 0:否)',
  `enabled_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用(1:是 0:否)',
  `employee_id` bigint(20) NOT NULL COMMENT '人员ID',
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

/*Data for the table `hand_jobs` */

insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (201,'J01','总经理',0,1,1);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (202,'J02','组长',0,1,2);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (203,'J03','总裁',0,1,3);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (204,'J04','董事长',0,1,4);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (205,'J05','CIO',0,1,5);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (206,'J06','CFO',0,1,6);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (207,'J07','CTO',0,1,7);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (208,'J08','技术顾问',0,1,8);
insert  into `hand_jobs`(`job_id`,`job_code`,`job_name`,`primary_flag`,`enabled_flag`,`employee_id`) values (209,'J09','高级技术顾问',0,1,6);

/*Table structure for table `hand_locations` */

CREATE TABLE `hand_locations` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `country` varchar(80) DEFAULT NULL COMMENT '国家',
  `province` varchar(80) DEFAULT NULL COMMENT '省',
  `city` varchar(80) DEFAULT NULL COMMENT '市',
  `address` varchar(240) NOT NULL COMMENT '详细地址',
  PRIMARY KEY (`location_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

/*Data for the table `hand_locations` */

insert  into `hand_locations`(`location_id`,`country`,`province`,`city`,`address`) values (1001,'中国','湖北省','武汉市','光谷软件园E10');
insert  into `hand_locations`(`location_id`,`country`,`province`,`city`,`address`) values (1002,'中国','上海市','上海市','青浦区汇联路33号');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
