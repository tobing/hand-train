package top.tobing.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobing.config.MemberConfig;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/20 10:04
 * @Description
 */
@Api(tags = "Member-测试模块")
@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberConfig memberConfig;

    @Autowired
    private DiscoveryClient client;

    /**
     * 获取配置信息（ConfigurationProperties方式）
     */
    @ApiOperation(value = "测试读取自定义配置", notes = "测试读取自定义配置ConfigurationProperties方式")
    @GetMapping("/config")
    public String test() {
        return "读取配置文件：username=" + memberConfig.getUsername() + " ,password:" + memberConfig.getPassword();
    }

    /**
     * 获取注册服务列表
     */
    @ApiOperation(value = "获取注册访问列表")
    @GetMapping("/discovery")
    public Object discovery() {
        List<String> list = client.getServices();
        System.out.println("**********" + list);
        List<ServiceInstance> srvList = client.getInstances("hand-gateway");
        for (ServiceInstance element : srvList) {
            System.out.println(element.getServiceId() + "\t" + element.getHost() + "\t" + element.getPort() + "\t"
                    + element.getUri());
        }
        return this.client;

    }

}
