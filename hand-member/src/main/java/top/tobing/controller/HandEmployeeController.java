package top.tobing.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.entity.HandEmployeeDO;
import top.tobing.service.HandEmployeeService;
import top.tobing.vo.HandEmployeeVO;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author tobing
 * @Date 2021/7/20 14:04
 * @Description
 */
@Api(tags = "Member-员工模块")
@RestController
@RequestMapping("/member")
public class HandEmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(HandEmployeeController.class);

    @Autowired
    private HandEmployeeService handEmployeeService;

    /**
     * 新增员工
     */
    @ApiOperation(value = "新增", notes = "新增一条员工信息")
    @PostMapping("/employee/save")
    public Result save(@ApiParam("新增的员工信息") @Valid @RequestBody HandEmployeeDO handEmployeeDO,
                       BindingResult bindingResult) {
        // 参数校验
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            Map<String, String> errorMessage = null;
            // 将错误信息转换为Map储存
            if (!CollectionUtils.isEmpty(fieldErrors)) {
                errorMessage = fieldErrors.stream().collect(
                        Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage, (oldKey, newKey) -> oldKey));
            }
            logger.warn("参数校验出错：{}", errorMessage.toString());
            return new Result(BziCodeEnum.PARAMETER_VALIDATION_ERROR.getCode(), BziCodeEnum.PARAMETER_VALIDATION_ERROR.getMsg(), errorMessage);
        }
        handEmployeeService.save(handEmployeeDO);
        return Result.ok();
    }

    /**
     * 更新员工
     */
    @ApiOperation(value = "全量更新", notes = "根据id全量更新员工信息")
    @PutMapping("/employee/update/{id}")
    public Result update(@ApiParam("员工id") @PathVariable("id") Long id,
                         @ApiParam("要更新的员工信息") @Valid @RequestBody HandEmployeeDO handEmployeeDO,
                         BindingResult bindingResult) {
        // 参数校验
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            Map<String, String> errorMessage = null;
            // 将错误信息转换为Map储存
            if (!CollectionUtils.isEmpty(fieldErrors)) {
                errorMessage = fieldErrors.stream().collect(
                        Collectors.toMap((item) -> {
                            return item.getField().toString() == null ? "" : item.getRejectedValue().toString();
                        }, (item) -> {
                            return item.getRejectedValue().toString() == null ? "" : item.getRejectedValue().toString();
                        }, (oldKey, newKey) -> oldKey));
            }
            logger.warn("参数校验出错：{}", errorMessage.toString());
            return new Result(BziCodeEnum.PARAMETER_VALIDATION_ERROR.getCode(), BziCodeEnum.PARAMETER_VALIDATION_ERROR.getMsg(), errorMessage);
        }
        handEmployeeDO.setEmployeeId(id);
        handEmployeeService.updateById(handEmployeeDO);
        return Result.ok();
    }

    /**
     * 更新员工【选择性】
     */
    @ApiOperation(value = "选择更新", notes = "根据id选择性更新，只更新传入的非空的字段")
    @PutMapping("/employee/update_select/{id}")
    public Result updateSelective(@ApiParam("员工id") @PathVariable("id") Long id,
                                  @ApiParam("要更新的员工信息") @RequestBody HandEmployeeDO handEmployeeDO) {
        handEmployeeDO.setEmployeeId(id);
        logger.info("选择更新信息，id={}", id);
        handEmployeeService.updateSelectiveById(handEmployeeDO);
        return Result.ok();
    }

    /**
     * 根据id查询员工
     */
    @ApiOperation(value = "查询一个", notes = "根据id查询员工详情")
    @GetMapping("/employee/one/{id}")
    public Result<HandEmployeeDTO> findById(@ApiParam("员工id") @PathVariable("id") Long id) {
        HandEmployeeDTO handEmployeeDTO = handEmployeeService.findById(id);
        return Result.ok(handEmployeeDTO);
    }

    /**
     * 条件查询员工，根据姓、名、部门编号查询
     */
    @ApiOperation(value = "条件搜索", notes = "根据条件搜索员工信息【模糊匹配姓名、精确匹配部门编号】")
    @GetMapping("/employee/like")
    public Result<List<HandEmployeeVO>> findByCondition(@ApiParam("搜索关键词") @RequestParam("key") String key) {
        logger.info("条件搜索，key={}", key);
        List<HandEmployeeVO> handEmployeeDTOList = handEmployeeService.findByCondition(key);
        return Result.ok(handEmployeeDTOList);
    }
}
