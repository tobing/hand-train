package top.tobing.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.tobing.common.Result;
import top.tobing.entity.HandDepartmentDO;
import top.tobing.service.HandDepartmentService;

/**
 * @Author tobing
 * @Date 2021/7/20 16:59
 * @Description
 */
@Api(tags = "Member-部门模块")
@RestController
@RequestMapping("/member")
public class HandDepartmentController {

    @Autowired
    private HandDepartmentService handDepartmentService;

    /**
     * 新增部门
     */
    @ApiOperation(value = "保存", notes = "保存部门信息")
    @PostMapping("/department/save")
    public Result save(@ApiParam("保存的部门信息") @RequestBody HandDepartmentDO handDepartmentDO) {
        handDepartmentService.save(handDepartmentDO);
        return Result.ok();
    }

    /**
     * 更新部门
     */
    @ApiOperation(value = "全量更新", notes = "根据id全量更新部门信息")
    @PutMapping("/department/update/{id}")
    public Result update(@ApiParam("部门id") @PathVariable("id") Long id,
                         @ApiParam("更新的部门信息") @RequestBody HandDepartmentDO handDepartmentDO) {
        handDepartmentDO.setDepartmentId(id);
        handDepartmentService.updateById(handDepartmentDO);
        return Result.ok();
    }
}
