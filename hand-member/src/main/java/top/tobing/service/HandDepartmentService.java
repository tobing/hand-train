package top.tobing.service;


import top.tobing.entity.HandDepartmentDO;

/**
 * @Author tobing
 * @Date 2021/7/20 13:41
 * @Description
 */
public interface HandDepartmentService {

    /**
     * 保存
     */
    void save(HandDepartmentDO handDepartmentDO);

    /**
     * 根据id更新【全量】
     */
    void updateById(HandDepartmentDO handDepartmentDO);
}
