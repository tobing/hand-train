package top.tobing.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.tobing.dao.HandDepartmentDao;
import top.tobing.entity.HandDepartmentDO;
import top.tobing.service.HandDepartmentService;

/**
 * @Author tobing
 * @Date 2021/7/20 13:41
 * @Description
 */
@Service
public class HandDepartmentServiceImpl implements HandDepartmentService {

    @Autowired
    private HandDepartmentDao handDepartmentDao;

    @Override
    public void save(HandDepartmentDO handDepartmentDO) {
        handDepartmentDao.save(handDepartmentDO);
    }

    @Override
    public void updateById(HandDepartmentDO handDepartmentDO) {
        handDepartmentDao.updateById(handDepartmentDO);
    }
}
