package top.tobing.service.impl;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.tobing.dao.HandEmployeeDao;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.entity.HandEmployeeDO;
import top.tobing.service.HandEmployeeService;
import top.tobing.vo.HandEmployeeVO;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/20 13:43
 */
@Service
public class HandEmployeeServiceImpl implements HandEmployeeService {

    @Autowired
    private HandEmployeeDao handEmployeeDao;


    @Override
    public HandEmployeeDTO findById(Long id) {
        HandEmployeeDO handEmployeeDO = handEmployeeDao.findById(id);
        if (handEmployeeDO != null) {
            HandEmployeeDTO handEmployeeDTO = new HandEmployeeDTO();
            BeanUtils.copyProperties(handEmployeeDO, handEmployeeDTO);
            return handEmployeeDTO;
        }
        return null;
    }

    @Override
    public void updateById(HandEmployeeDO handEmployeeDO) {
        handEmployeeDao.updateById(handEmployeeDO);
    }

    @Override
    public void save(HandEmployeeDO handEmployeeDO) {
        handEmployeeDao.save(handEmployeeDO);
    }

    @Override
    public List<HandEmployeeVO> findByCondition(String key) {
        return handEmployeeDao.findByCondition(key);
    }

    @Override
    public void updateSelectiveById(HandEmployeeDO handEmployeeDO) {
        handEmployeeDao.updateByIdSelective(handEmployeeDO);
    }
}
