package top.tobing.service;


import top.tobing.dto.HandEmployeeDTO;
import top.tobing.entity.HandEmployeeDO;
import top.tobing.vo.HandEmployeeVO;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/20 13:43
 */
public interface HandEmployeeService {

    /**
     * 根据id查询HandEmployeeDTO
     */
    HandEmployeeDTO findById(Long id);

    /**
     * 根据id更新员工信息
     */
    void updateById(HandEmployeeDO handEmployeeDO);

    /**
     * 保存员工信息
     */
    void save(HandEmployeeDO handEmployeeDO);

    /**
     * 条件查询数据
     *
     * @return
     */
    List<HandEmployeeVO> findByCondition(String key);

    /**
     * 根据id更新员工信息【选择性】
     */
    void updateSelectiveById(HandEmployeeDO handEmployeeDO);
}
