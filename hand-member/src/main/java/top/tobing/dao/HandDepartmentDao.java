package top.tobing.dao;

import lombok.Data;
import org.apache.ibatis.annotations.Mapper;
import top.tobing.entity.HandDepartmentDO;

/**
 * @Author tobing
 * @Date 2021/7/20 13:41
 * @Description 部门Dao
 */
@Mapper
public interface HandDepartmentDao {

    void save(HandDepartmentDO handDepartmentDO);

    void updateById(HandDepartmentDO handDepartmentDO);

}
