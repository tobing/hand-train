package top.tobing.dao;

import lombok.Data;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.entity.HandEmployeeDO;
import top.tobing.vo.HandEmployeeVO;

import java.util.Date;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/20 13:43
 * @Description 员工Dao
 */
@Mapper
public interface HandEmployeeDao {

    HandEmployeeDO findById(@Param("id") Long id);

    void updateById(HandEmployeeDO handEmployeeDO);

    void updateByIdSelective(HandEmployeeDO handEmployeeDO);

    void save(HandEmployeeDO handEmployeeDO);

    List<HandEmployeeVO> findByCondition(@Param("key") String key);
}
