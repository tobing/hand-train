package top.tobing.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author tobing
 * @Date 2021/7/20 18:31
 * @Description 雇员综合详细信息VO
 */
@ApiModel(value = "员工VO")
@Data
public class HandEmployeeVO {
    /**
     * 名
     */
    @ApiModelProperty(value = "名", example = "尚福")
    private String firstName;
    /**
     * 姓
     */
    @ApiModelProperty(value = "姓", example = "梁")
    private String lastName;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱", example = "tobing@hand-china.com")
    private String email;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别", example = "男")
    private String sex;
    /**
     * 电话号码
     */
    @ApiModelProperty(value = "电话号码", example = "11213121312")
    private String phoneNum;
    /**
     * 部门编号
     */
    @ApiModelProperty(value = "部门编号", example = "ACID")
    private String departmentCode;
    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称", example = "技术部")
    private String departmentName;
    /**
     * 部门所在地址
     */
    @ApiModelProperty(value = "部门所在地址", example = "四川省成都市")
    private String detailAddress;
}
