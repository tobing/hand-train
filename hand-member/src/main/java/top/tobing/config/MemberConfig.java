package top.tobing.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author tobing
 * @Date 2021/7/20 13:31
 * @Description
 */
@Data
@Component
@ConfigurationProperties(prefix = "hand")
public class MemberConfig {
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
}
