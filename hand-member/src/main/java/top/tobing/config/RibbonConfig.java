package top.tobing.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author tobing
 * @Date 2021/7/20 15:29
 * @Description 负载均衡规则配置-随机
 */
@Configuration
public class RibbonConfig {
    @Bean
    public IRule iRule() {
        return new RandomRule();
    }
}
