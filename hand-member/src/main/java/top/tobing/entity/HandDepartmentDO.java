package top.tobing.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author tobing
 * @Date 2021/7/20 13:41
 * @Description 部门DO
 */

@ApiModel(value = "部门DO")
@Data
public class HandDepartmentDO {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", example = "1")
    private Long departmentId;
    /**
     * 部门编号
     */
    @ApiModelProperty(value = "部门编号", example = "ACID")
    private String departmentCode;
    /**
     * 部门名称
     */
    @ApiModelProperty(value = "部门名称", example = "技术部")
    private String departmentName;
    /**
     * 部门主管
     */
    @ApiModelProperty(value = "部门主管id", example = "1")
    private Long managerId;
    /**
     * 部门地址
     */
    @ApiModelProperty(value = "部门地址id", example = "1")
    private Long locationId;
}
