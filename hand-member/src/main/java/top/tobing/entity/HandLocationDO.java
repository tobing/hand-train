package top.tobing.entity;

import lombok.Data;

/**
 * @Author tobing
 * @Date 2021/7/20 13:44
 * @Description 地区DO
 */
@Data
public class HandLocationDO {

    /**
     * 主键
     */
    private Long locationId;
    /**
     * 国家
     */
    private String country;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 详细地址
     */
    private String address;
}
