package top.tobing.entity;

import lombok.Data;

/**
 * @Author tobing
 * @Date 2021/7/20 13:44
 * @Description 工作DO
 */
@Data
public class HandJobDO {

    /**
     * 主键
     */
    private Long jobId;
    /**
     * 职位编号
     */
    private String jobCode;
    /**
     * 职位名称
     */
    private String jobName;
    /**
     * 是否主岗
     */
    private Boolean primaryFlag;
    /**
     * 是否启用
     */
    private Boolean enabledFlag;
    /**
     * 人员ID
     */
    private Long employeeId;

}
