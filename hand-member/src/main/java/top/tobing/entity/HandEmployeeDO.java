package top.tobing.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * @Author tobing
 * @Date 2021/7/20 13:43
 * @Description 员工DO
 */
@Data
@ApiModel("员工DO")
public class HandEmployeeDO {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", example = "1")
    private Long employeeId;
    /**
     * 人员编号
     */
    @NotEmpty(message = "员工编号必须填写")
    @Length(min = 6, max = 18, message = "员工编号必须在6-18个字符之间")
    @ApiModelProperty(value = "员工编号", example = "1234")
    private String employeeNum;
    /**
     * 名
     */
    @NotEmpty(message = "名不能为空")
    @Length(min = 1, max = 200, message = "名必须在1-200个字符之间")
    @ApiModelProperty(value = "名", example = "尚福")
    private String firstName;
    /**
     * 姓
     */
    @NotEmpty(message = "姓不能为空")
    @Length(min = 1, max = 100, message = "姓必须在1-100个字符之间")
    @ApiModelProperty(value = "姓", example = "梁")
    private String lastName;
    /**
     * 性别
     */
    @Length(min = 0, max = 1, message = "性别字段长度最长为1个字符")
    @ApiModelProperty(value = "性别", example = "男")
    private String sex;
    /**
     * 电话号码
     */
    @Pattern(regexp = "^(13[0-9]|14[5|7]|15[0|1|2|3|4|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$", message = "手机格式不正确")
    @ApiModelProperty(value = "电话号码", example = "13454345434")
    private String phoneNum;
    /**
     * 电子邮件
     */
    @Email(message = "电子邮箱格式不正确")
    @ApiModelProperty(value = "电子邮箱", example = "tobing@hand-china.com")
    private String email;
    /**
     * 入职日期
     */
    @ApiModelProperty(value = "入职日期", example = "2021-01-11")
    private Date hireDate;
    /**
     * 部门ID
     */
    @ApiModelProperty(value = "部门ID", example = "1")
    private Long departmentId;
    /**
     * 员工状态
     */
    @ApiModelProperty(value = "员工状态", example = "ACID")
    private String statusCode;
}
