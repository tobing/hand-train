package top.tobing;

import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * @Author tobing
 * @Date 2021/7/19 20:43
 * @Description
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties
@EnableFeignClients
@CrossOrigin
@RibbonClient(name = "HandMember", configuration = RandomRule.class)
public class HandMemberApplication {
    public static void main(String[] args) {
        SpringApplication.run(HandMemberApplication.class, args);
    }
}
