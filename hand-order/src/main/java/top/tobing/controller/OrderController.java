package top.tobing.controller;

import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.feign.MemberFeignService;
import top.tobing.vo.OrderVO;

import java.util.Date;
import java.util.UUID;

/**
 * @Author tobing
 * @Date 2021/7/20 10:37
 * @Description
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private MemberFeignService memberFeignService;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/member/remote/{memberId}")
    public Result<OrderVO> testRemoteApi(@PathVariable("memberId") Long memberId) {
        OrderVO orderVO = new OrderVO();
        orderVO.setOrderSn(UUID.randomUUID().toString());
        orderVO.setCreateTime(new Date());
        // 调用远程服务获取顾客信息
        Result<HandEmployeeDTO> handEmployeeDTOResult = memberFeignService.findById(memberId);
        if (BziCodeEnum.SUCCESS.getCode().equals(handEmployeeDTOResult.getCode())) {
            orderVO.setEmployee(handEmployeeDTOResult.getData());
            return Result.ok(orderVO);
        }
        return Result.error(BziCodeEnum.REMOTE_ERROR.getCode(), BziCodeEnum.REMOTE_ERROR.getMsg());
    }

    @RequestMapping("/member/loadbalance")
    public Result<OrderVO> testLoadLoadBalanced() {
        Result object = restTemplate.getForObject("http://hand-member/member/employee/1", Result.class);
        return object;
    }


}
