package top.tobing.feign.impl;

import org.springframework.stereotype.Component;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.feign.MemberFeignService;

/**
 * @Author tobing
 * @Date 2021/7/20 14:41
 * @Description
 */
//@Component()
public class MemberFeignServiceImpl implements MemberFeignService {
    @Override
    public Result<HandEmployeeDTO> findById(Long id) {
        return Result.error(BziCodeEnum.REMOTE_ERROR.getCode(), BziCodeEnum.REMOTE_ERROR.getMsg());
    }
}
