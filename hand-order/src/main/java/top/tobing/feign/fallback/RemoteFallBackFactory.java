package top.tobing.feign.fallback;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.feign.MemberFeignService;

/**
 * @Author tobing
 * @Date 2021/7/21 10:20
 * @Description
 */
@Component
public class RemoteFallBackFactory implements FallbackFactory<MemberFeignService> {

    private static final Logger logger = LoggerFactory.getLogger(RemoteFallBackFactory.class);


    @Override
    public MemberFeignService create(Throwable throwable) {
        logger.error("远程调用发生异常：", throwable);
        return new MemberFeignService() {
            @Override
            public Result<HandEmployeeDTO> findById(Long id) {
                return Result.error(BziCodeEnum.REMOTE_ERROR.getCode(), BziCodeEnum.REMOTE_ERROR.getMsg());
            }
        };
    }
}
