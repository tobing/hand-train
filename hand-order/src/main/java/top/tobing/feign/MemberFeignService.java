package top.tobing.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import top.tobing.common.Result;
import top.tobing.dto.HandEmployeeDTO;
import top.tobing.feign.fallback.RemoteFallBackFactory;
import top.tobing.feign.impl.MemberFeignServiceImpl;

/**
 * @Author tobing
 * @Date 2021/7/20 10:38
 * @Description
 */
@FeignClient(value = "hand-member", fallbackFactory = RemoteFallBackFactory.class)
public interface MemberFeignService {
    @GetMapping("/member/employee/one/{id}")
    public Result<HandEmployeeDTO> findById(@PathVariable("id") Long id);
}
