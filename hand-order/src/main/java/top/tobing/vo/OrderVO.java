package top.tobing.vo;

import lombok.Data;
import top.tobing.dto.HandEmployeeDTO;

import java.util.Date;

/**
 * @Author tobing
 * @Date 2021/7/20 14:55
 * @Description
 */
@Data
public class OrderVO {
    /**
     * 订单号
     */
    private String orderSn;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 顾客信息
     */
    private HandEmployeeDTO employee;
}
