package top.tobing.common;

/**
 * @Author tobing
 * @Date 2021/7/20 11:03
 * @Description
 */
public enum BziCodeEnum {

    // 通用错误码
    SUCCESS(200, "success"),
    SERVER_ERROR(50000, "服务端异常"),
    REMOTE_ERROR(50001, "远程调用异常，正在熔断处理，请稍后！"),
    PARAMETER_VALIDATION_ERROR(50002, "参数校验异常，请检查！"),
    UNAUTH_ERROR(50003, "认证异常，无权访问此页面！");


    private Integer code;
    private String msg;


    BziCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "BziCodeEnum{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
