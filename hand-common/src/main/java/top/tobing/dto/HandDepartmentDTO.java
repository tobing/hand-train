package top.tobing.dto;

import lombok.Data;

/**
 * @Author tobing
 * @Date 2021/7/20 13:41
 * @Description 部门DO
 */
@Data
public class HandDepartmentDTO {
    /**
     * 主键
     */
    private Long departmentId;
    /**
     * 部门编号
     */
    private String departmentCode;
    /**
     * 部门名称
     */
    private String departmentName;
    /**
     * 部门主管
     */
    private Long managerId;
    /**
     * 部门地址
     */
    private Long locationId;
}
