package top.tobing.dto;

import lombok.Data;

import java.util.Date;

/**
 * @Author tobing
 * @Date 2021/7/20 13:43
 * @Description 员工DO
 */
@Data
public class HandEmployeeDTO {

    /**
     * 主键
     */
    private String employeeId;
    /**
     * 人员编号
     */
    private String employeeNum;
    /**
     * 名
     */
    private String firstName;
    /**
     * 姓
     */
    private String lastName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 电话号码
     */
    private String phoneNum;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * 入职日期
     */
    private Date hireDate;
    /**
     * 部门ID
     */
    private Long departmentId;
    /**
     * 员工状态
     */
    private String statusCode;
}
