package top.tobing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @Author tobing
 * @Date 2021/7/20 9:35
 * @Description
 */
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class HandConfigServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(HandConfigServerApplication.class, args);
    }
}
