package top.tobing.exception;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;


/**
 * @Author tobing
 * @Date 2021/7/22 10:08
 * @Description 网关异常拦截，将返回结果统一
 * 网关的拦截与其他web服务有所不同，需要特别注意，下面用法参考一下连接：
 * https://www.programmersought.com/article/16386520536/
 */
@Component
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {

    @Autowired
    private ObjectMapper objectMapper;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        ServerHttpResponse response = exchange.getResponse();

        if (!response.isCommitted()) {
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            if (ex instanceof ResponseStatusException) {
                response.setStatusCode(((ResponseStatusException) ex).getStatus());
            }

            return response
                    .writeWith(Mono.fromSupplier(() -> {
                        DataBufferFactory bufferFactory = response.bufferFactory();
                        try {
                            logger.error("服务端异常！");
                            return bufferFactory.wrap(
                                    objectMapper.writeValueAsBytes(
                                            new Result(BziCodeEnum.SERVER_ERROR.getCode()
                                                    , BziCodeEnum.SERVER_ERROR.getMsg())));
                        } catch (Exception e) {
                            logger.warn("发生异常：", ex);
                            return bufferFactory.wrap(new byte[0]);
                        }
                    }));
        } else {
            return Mono.error(ex);
        }

    }
}
