package top.tobing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import top.tobing.config.HostAddrKeyResolver;

/**
 * @Author tobing
 * @Date 2021/7/19 20:41
 * @Description
 */
@SpringBootApplication
@EnableDiscoveryClient
public class HandGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(HandGatewayApplication.class, args);
    }


    @Bean
    public HostAddrKeyResolver hostAddrKeyResolver() {
        return new HostAddrKeyResolver();
    }
}
