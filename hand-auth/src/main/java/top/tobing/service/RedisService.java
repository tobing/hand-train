package top.tobing.service;

/**
 * @Author tobing
 * @Date 2021/7/27 16:00
 * @Description
 */
public interface RedisService {
    /**
     * 存储数据
     */
    void set(String prefix, String key, Object value);

    /**
     * 获取数据
     */
    Object get(String prefix, String key);

    /**
     * 设置超期时间
     */
    boolean expire(String prefix, String key, long expire);

    /**
     * 删除数据
     */
    void remove(String prefix, String key);
}
