package top.tobing.service;

import top.tobing.entity.UserDO;

/**
 * @Author tobing
 * @Date 2021/7/27 13:48
 * @Description
 */
public interface UserService {

    /**
     * 根据用户名查询一个用户
     */
    UserDO findOneByUsername(String username);
}
