package top.tobing.service;

import top.tobing.entity.PermissionDO;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/27 13:48
 * @Description
 */
public interface PermissionService {

    /**
     * 根据用户id查询用户权限
     */
    List<PermissionDO> listPermissionValueByUserId(String userId);
}
