package top.tobing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import top.tobing.service.RedisService;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/7/27 16:00
 * @Description
 */
@Service
public class RedisServiceImpl implements RedisService {
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void set(String prefix, String key, Object value) {
        redisTemplate.opsForValue().set(prefix + key, value);
    }

    @Override
    public Object get(String prefix, String key) {
        return redisTemplate.opsForValue().get(prefix + key);
    }

    @Override
    public boolean expire(String prefix, String key, long expire) {
        return redisTemplate.expire((prefix + key), expire, TimeUnit.SECONDS);
    }

    @Override
    public void remove(String prefix, String key) {
        redisTemplate.delete(prefix + key);
    }

}
