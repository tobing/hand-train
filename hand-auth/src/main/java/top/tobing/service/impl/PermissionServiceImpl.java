package top.tobing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.tobing.dao.PermissionDao;
import top.tobing.entity.PermissionDO;
import top.tobing.service.PermissionService;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/27 13:48
 * @Description
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public List<PermissionDO> listPermissionValueByUserId(String userId) {
        return permissionDao.listPermissionValueByUserId(userId);
    }
}
