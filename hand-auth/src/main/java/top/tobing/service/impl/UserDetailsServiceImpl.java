package top.tobing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import top.tobing.entity.PermissionDO;
import top.tobing.entity.SecurityUser;
import top.tobing.entity.UserDO;
import top.tobing.service.PermissionService;
import top.tobing.service.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author tobing
 * @Date 2021/7/27 11:31
 * @Description 执行用户认证
 */

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permissionService;

    /**
     * 根据username到数据查询用户数据，封装用户权限信息
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户权限信息
        UserDO userDO = userService.findOneByUsername(username);
        if (userDO == null) {
            throw new UsernameNotFoundException("用户号不存在");
        }
        // 获取授权列表
        List<PermissionDO> permissionDOList = permissionService.listPermissionValueByUserId(userDO.getId());
        // 封装用户授权信息
        SecurityUser securityUser = new SecurityUser();
        securityUser.setUserDO(userDO);
        if (!CollectionUtils.isEmpty(permissionDOList)) {
            Set<String> permissionListValues = permissionDOList
                    // 去除null元素
                    .stream().filter(Objects::nonNull)
                    // 提取权限信息
                    .map(PermissionDO::getPermissionValue)
                    // 组装为Set
                    .collect(Collectors.toSet());
            securityUser.setPermissionList(permissionListValues);
        }
        return securityUser;
    }
}
