package top.tobing.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.tobing.dao.UserDao;
import top.tobing.entity.UserDO;
import top.tobing.service.UserService;

/**
 * @Author tobing
 * @Date 2021/7/27 13:48
 * @Description
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public UserDO findOneByUsername(String username) {
        QueryWrapper<UserDO> wrapper = new QueryWrapper<UserDO>().eq("username", username);
        return userDao.selectOne(wrapper);
    }
}
