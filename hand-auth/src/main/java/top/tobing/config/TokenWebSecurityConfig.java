package top.tobing.config;

import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import top.tobing.constant.HandAuthConstant;
import top.tobing.security.*;
import top.tobing.service.RedisService;

/**
 * @Author tobing
 * @Date 2021/7/27 10:02
 * @Description token 认证配置
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class TokenWebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private RedisService redisService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // token模式下关闭csrf
        http.csrf().disable()
                // 指定认证异常处理类
                .exceptionHandling().authenticationEntryPoint(new TokenAuthenticationEntryPoint())
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                // 指定注销url地址
                .logout().logoutUrl(HandAuthConstant.AUTH_LOGOUT_URL)
                // 指定注销处理器
                .addLogoutHandler(new TokenLogoutHandler())
                .and()
                // 指定登录Filter
                .addFilter(new TokenUsernamePasswordAuthFilter(authenticationManager(), tokenManager, redisService))
                // 指定授权Filter
                .addFilter(new TokenBasicAuthFilter(authenticationManager(), tokenManager, redisService))
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 指定认证处理接口
        auth.userDetailsService(userDetailsService)
                // 指定密码编码器
                .passwordEncoder(passwordEncoder);
    }

    /**
     * 将BCryptPasswordEncoder作为默认的编码器
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
