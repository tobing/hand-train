package top.tobing.constant;

/**
 * @Author tobing
 * @Date 2021/7/27 10:27
 * @Description 认证服务常量
 */
public class HandAuthConstant {
    /**
     * 注销URL
     */
    public static final String AUTH_LOGOUT_URL = "/auth/logout";
    /**
     * 登录URL
     */
    public static final String AUTH_LOGIN_URL = "/auth/login";
    /**
     * 登录请求方法
     */
    public static final String AUTH_LOGIN_METHOD_TYPE = "POST";
    /**
     * token请求头字段
     */
    public static final String AUTH_REQUEST_HEADER_FIELD = "token";

    public static final String AUTH_REDIS_PREFIX = "hand:auth:";


}
