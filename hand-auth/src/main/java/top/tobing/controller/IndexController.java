package top.tobing.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.tobing.common.Result;

/**
 * @Author tobing
 * @Date 2021/7/27 13:54
 * @Description
 */
@RestController()
@RequestMapping("/index")
public class IndexController {

    @GetMapping("/info")
    @PreAuthorize("hasAnyAuthority('user.update')")
    public Result info() {
        return Result.ok();

    }

    @GetMapping("/menu")
    public Result getMenu() {
        return Result.ok();

    }

    public Result logout() {
        return Result.ok();
    }
}
