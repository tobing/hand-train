package top.tobing.entity;

import lombok.Data;

/**
 * @Author tobing
 * @Date 2021/7/27 10:53
 * @Description 用户登录信息类
 */
@Data
public class UserDTO {
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 用户签名
     */
    private String token;
}
