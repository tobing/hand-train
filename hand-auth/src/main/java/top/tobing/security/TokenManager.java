package top.tobing.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author tobing
 * @Date 2021/7/27 10:45
 * @Description Token 管理器
 */
@Component
public class TokenManager {
    /**
     * token 过期时间
     */
    @Value("${jwt.expiration}")
    private long tokenExpiration;
    /**
     * 签名密钥
     */
    @Value("${jwt.secret}")
    private String tokenSignKey;


    /**
     * 根据 username 生成 token
     */
    public String createToken(String username) {
        return Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                .signWith(SignatureAlgorithm.HS512, tokenSignKey)
                .compact();
    }

    /**
     * 移除token（jwttoken无需删除，客户端扔掉即可）
     */
    public void removeToken(String token) {
    }

    /**
     * 从 token 中获取 username
     */
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(tokenSignKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }
}
