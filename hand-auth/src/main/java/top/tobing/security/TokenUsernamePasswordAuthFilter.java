package top.tobing.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;
import top.tobing.constant.HandAuthConstant;
import top.tobing.entity.SecurityUser;
import top.tobing.entity.UserDTO;
import top.tobing.service.RedisService;
import top.tobing.util.ResponseUtil;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author tobing
 * @Date 2021/7/27 10:34
 * @Description 认证处理过滤器
 * 校验用户是否为系统合法用户
 */
public class TokenUsernamePasswordAuthFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenUsernamePasswordAuthFilter.class);

    private AuthenticationManager authenticationManager;
    private TokenManager tokenManager;
    private RedisService redisService;

    public TokenUsernamePasswordAuthFilter(AuthenticationManager authenticationManager,
                                           TokenManager tokenManager,
                                           RedisService redisService) {
        this.authenticationManager = authenticationManager;
        this.tokenManager = tokenManager;
        this.redisService = redisService;
        this.setPostOnly(false);
        // 设置登录接口地址以及登录请求方式
        this.setRequiresAuthenticationRequestMatcher(
                new AntPathRequestMatcher(HandAuthConstant.AUTH_LOGIN_URL, HandAuthConstant.AUTH_LOGIN_METHOD_TYPE));
    }

    /**
     * 认证过程
     * 从请求中获取用户填写的username、password封装为UserDTO，传递给UserDetailsService处理
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            UserDTO userDTO = new ObjectMapper().readValue(request.getInputStream(), UserDTO.class);
            return authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            LOGGER.warn("用户认证出现异常：{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 认证成功回调函数
     * 1、根据认证成功的用户号，生成token返回前端
     * 2、以用户名为键，用户权限信息为值，将用户的权限信息保存到redis中
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        SecurityUser securityUser = (SecurityUser) authResult.getPrincipal();
        // 保存权限信息到Redis
        List<String> authList = null;
        if (!CollectionUtils.isEmpty(securityUser.getAuthorities())) {
            authList = securityUser.getAuthorities().stream()
                    // 过滤null值
                    .filter(item -> !StringUtils.isEmpty(item))
                    // 提取权限信息
                    .map(GrantedAuthority::getAuthority)
                    // 转换为List
                    .collect(Collectors.toList());
        }
        redisService.set(HandAuthConstant.AUTH_REDIS_PREFIX, securityUser.getUsername(), authList);
        // 返回token信息
        String token = tokenManager.createToken(securityUser.getUsername());
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        ResponseUtil.out(response, Result.ok(map));
    }

    /**
     * 认证失败回调函数
     * 返回认证错误状态码
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        ResponseUtil.out(response, Result.error(BziCodeEnum.UNAUTH_ERROR.getCode(), BziCodeEnum.UNAUTH_ERROR.getMsg()));
    }
}
