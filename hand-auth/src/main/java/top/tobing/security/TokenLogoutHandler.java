package top.tobing.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author tobing
 * @Date 2021/7/27 10:29
 * @Description Token用户注销处理器
 */
public class TokenLogoutHandler implements LogoutHandler {

    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 用户注销
     * 注销时，需要将redis中的用户信息删除
     */
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String token = request.getHeader("token");
        if (token != null) {
            tokenManager.removeToken(token);
            String username = tokenManager .getUsername(token);
            redisTemplate.delete(username);
        }
    }
}
