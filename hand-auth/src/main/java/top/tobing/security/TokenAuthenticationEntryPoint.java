package top.tobing.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import top.tobing.common.BziCodeEnum;
import top.tobing.common.Result;
import top.tobing.util.ResponseUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author tobing
 * @Date 2021/7/27 10:06
 * @Description Token认证异常处理类
 */
public class TokenAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        ResponseUtil.out(response, Result.error(BziCodeEnum.UNAUTH_ERROR.getCode(), BziCodeEnum.UNAUTH_ERROR.getMsg()));
    }
}
