package top.tobing.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.tobing.constant.HandAuthConstant;
import top.tobing.service.RedisService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/27 10:37
 * @Description 用户授权处理过滤器
 * 校验用户是否访问某一接口的权限
 */
public class TokenBasicAuthFilter extends BasicAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenBasicAuthFilter.class);


    private TokenManager tokenManager;
    private RedisService redisService;

    public TokenBasicAuthFilter(AuthenticationManager authenticationManager,
                                TokenManager tokenManager,
                                RedisService redisService) {
        super(authenticationManager);
        this.tokenManager = tokenManager;
        this.redisService = redisService;
    }

    /**
     * 获取指定请求的授权信息
     * 获取请求信息中的认证信息，具体逻辑见 {@link TokenBasicAuthFilter#getAuthentication(HttpServletRequest)}
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        LOGGER.info("请求授权URL：{}", request.getRequestURI());
        //获取当前认证成功用户权限信息
        UsernamePasswordAuthenticationToken authenticationToken = getAuthentication(request);
        // 存在认证信息，则将认证信息写到上下文环境
        if (authenticationToken != null) {
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }

        chain.doFilter(request, response);
    }

    /**
     * 获取用户请求中的授权信息
     * 1、获取用户token
     * 2、提取用户token中的username
     * 3、根据username到redis中获取用户的信息
     * 4、封住用户信息（具有的权限）返回UsernamePasswordAuthenticationToken对象
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        // 获取request中header的token信息
        String token = request.getHeader(HandAuthConstant.AUTH_REQUEST_HEADER_FIELD);
        if (StringUtils.isEmpty(token)) {
            return null;
        }
        // 从token中提取username
        String username = tokenManager.getUsername(token);
        if (StringUtils.isEmpty(username)) {
            return null;
        }
        // 根据username到redis中查询用户权限相关信息
        List<String> permissionValueList = (List<String>) redisService.get(ALREADY_FILTERED_SUFFIX, username);
        // 封装权限信息
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        if (!CollectionUtils.isEmpty(permissionValueList)) {
            for (String permissionValue : permissionValueList) {
                if (!StringUtils.isEmpty(permissionValue)) {
                    SimpleGrantedAuthority authority = new SimpleGrantedAuthority(permissionValue);
                    authorities.add(authority);
                }
            }
        }
        return new UsernamePasswordAuthenticationToken(username, token, authorities);
    }
}
