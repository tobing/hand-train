package top.tobing.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.tobing.entity.RoleDO;

/**
 * @Author tobing
 * @Date 2021/7/27 13:43
 * @Description
 */
@Repository
public interface RoleDao extends BaseMapper<RoleDO> {
}
