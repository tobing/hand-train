package top.tobing.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import top.tobing.entity.UserDO;

/**
 * @Author tobing
 * @Date 2021/7/27 13:44
 * @Description
 */
@Repository
public interface UserDao extends BaseMapper<UserDO> {
}
