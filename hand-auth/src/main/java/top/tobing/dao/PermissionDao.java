package top.tobing.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.tobing.entity.PermissionDO;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/27 13:43
 * @Description
 */
@Repository
public interface PermissionDao extends BaseMapper<PermissionDO> {

    /**
     * 根据userId查询相应权限
     */
    List<PermissionDO> listPermissionValueByUserId(@Param("userId") String userId);
}
