package top.tobing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author tobing
 * @Date 2021/7/23 13:02
 * @Description
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("top.tobing.dao")
public class HandAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(HandAuthApplication.class, args);
    }
}
