package top.tobing;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Author tobing
 * @Date 2021/7/27 15:05
 * @Description
 */
@SpringBootTest
public class HandAuthTest {

    @Test
    public void testBCryptPasswordEncoder() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String tobing = passwordEncoder.encode("tobing");
        System.out.println(tobing);
    }

}
