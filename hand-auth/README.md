# 认证与授权

认证与授权是一个系统中不可绕开的关键一环，认证和授权保证了系统的访问的安全性、合法性。对于不同架构，其他特点同步，其认证和授权的方式也不同。下面主要针对常见的两种架构，剖析其认证和授权的过程。

+ 认证（Authentication）：指验证你是谁，因此需要提供用户名和密码来机型认证
+ 授权（Authorization）：指验证你可以干什么，如对某些资源具有CRUD权限，授权需要在认证通过之后

## 传统单体应用的认证与授权

### 简介

在单体应用中，整个应用就是一个进程，应用中的安全模块用来实现用户的认证与授权。

当用户登录时，安全模块会认证用户身份。如果用户合法，就会给用户创建一个session，其中session保存了用户登录信息，如用户名、角色、权限等。随后服务端会返回与session唯一关联的session id，这个session id以cookie的形式保存在客户端。

以后的每次请求到来，服务端会验证客户端的cookie中的session id，进而获取到用户的登录信息，验证用户的合法性，这样就不需要每次都验证用户名和密码。

![Monolithic_application_user_login_authentication](https://gitee.com/tobing/imagebed/raw/4be35f00d081e39b3aa66bd011ad47f3fac268ef/Monolithic_application_user_login_authentication.png)

当客户端访问应用时，session id会被HTTP请求携带。通常安全模块会通过拦截器，对用户请求进行拦截，判断其中是否存在session id。如果session id存在，就表明用户已经登录，这时只需要查询对用用户的角色以及其拥有的权限。

![Monolithic_user_request_authorization](https://gitee.com/tobing/imagebed/raw/master/Monolithic_user_request_authorization.png)

## 微服务架构应用的认证与授权

### 简介

在微服务架构下，整个应用被拆分为多个小的服务进程，每个服务进程实现原来单体应用中的一个模块业务。业务拆分之后，每个微服务的访问都需要进行认证和授权。如果仍然采用单体应用下的解决方案，将会存在以下问题：

+ 每个微服务都需要处理认证和授权的逻辑，经过可以通过代码库重用，但是仍然会带来所有微服务依赖于特定的代码库以及版本。这样一来就会影响微服务语言/框架选择的伸缩性。
+ 微服务应该遵循单一责任原则。每个微服务应该只处理自己单独的业务逻辑。认证和授权的业务逻辑不应该由每个微服务来实现。
+ HTTP协议是无状态协议。对于服务端而言，用户的每次HTTP都是独立的。无状态意味着服务器可以根据需要向集群中任意一节点转发客户端的请求。HTTP的无状态设计对负载均衡有明显的好处。因为请求的无状态性，使得用户请求可以被转发到任何的服务器中。这样对不需要身份认证的服务，如浏览新闻页，没有问题。但是，大部分服务，如在线购物以及企业后台管理系统，需要对用户进行身份认证。因此在使用HTTP协议时，保存用户的状态是很有必要的，因为这样可以避免每次请求都需要进行验证。传统的方式是在客户端使用session保存用户状态。但由于服务器是有状态的，因此这样会影响服务器的横向扩展。
+ 微服务架构中的认证和授权设计的场景比较复杂，设计到用户访问微服务应用、第三方应用访问微服务应用、第三方应用相互访问，在每一种场景下，需要以下的认证授权方案来保证应用程序的安全性。

> 补充：横向扩展与纵向扩展
>
> + 横向扩展/水平扩展：用更多的节点来支持更大量的请求。
> + 纵向扩展/垂直扩展：扩展一个点的能力来支持更大的请求。

### 微服务认证授权解决方案

#### 分布式Session

为了充分利用微服务架构的优势实现微服务的可扩展性和弹性，微服务最好是无状态的。

这种解决方案可以通过不同方式应用：

**Sticky session**

确保来自特定用户的所有且能够都会发送给处理该用户对应的第一个请求的同一台服务器，从而确保会话数据对于某个用户始终是正确的。但是这种方式依赖于均衡负载器，只能满足横向扩展的集群场景，但是当负载均衡器因为任何原因被迫将用户转移到不同的服务器时，用户的所有会话数据将会丢失。

**Session replication**

每个实例保存所有会话数据，并且通过网络同步。同步数据会导致网络开销，只要会话数据发送修改，将需要将数据同步到所有其他计算机中。实例越多，同步带来的网络带宽越多。

**Centralized session storage**

集中式管理用户的会话信息，如统一存放在Redis中，这样一来可以确保所有的微服务可以读取到相同的会话数据。在某些场景下，这种方案很好用，并且用户登录状态不透明。它是一种高可用和高可扩展性的解决方案。但这种解决方案的缺点就是需要保护机制来保护共享会话数据的安全访问。

#### 客户端Token

传统的方式是在客户端使用会话来保存用户状态。因为服务器是有状态的，因此对服务器的横向扩展有影响。微服务架构中推荐使用Token来记录用户的登录状态。

Token和Session的主要区别在于储存的不同：Token由用户持有，通常以Cookie的形式储存在浏览器中。Token保存着用户的身份信息，每次先服务器发送请求时，服务器就可以确定访问者的身份，并确定是否可以访问所有请求的资源。

Token用于指示用户身份，因此需要对Token的内容进行加密，避免请求者或第三方篡改。JWT（Json Web Token）是一个开放标准（RFC 7519），它定义了Token格式，定义了Token内容，对其进行了加密，并提供了各种语言的库。

Token的结构非常简单，主要有三部分组成：

**Header**

头部包含了类型，为固定值JWT，接着是JWT使用的Hash算法。

```json
{
    "typ": "JWT",	
    "alg": "HS256"
}
```

**Payload**

负载部分包含了标准信息，如userId、过期时间和用户名等。这里还可以添加用户角色和用户定义的信息。

```json
{ 
    "id": 123, 
    "name": "Mena Meseha",
    "is_admin": true,
    "expire": 1558213420 
}
```

**Signature**

Token的签名用来给客户端验证令牌的身份以及验证消息在传输过程中有没有被篡改。

Header、Payload、Signature三部分最终会使用Base64机型编码，以`.`分隔，最终组恒返回给客户端。

通过使用令牌进行用户身份验证，服务器不会保存用户状态。每次客户端请求令牌时，客户端都需要将其发送到服务器进行身份验证。

使用Token时，用户认证的基本流程如下图所示：

![1527305891424](https://gitee.com/tobing/imagebed/raw/master/1527305891424.png)

#### 单点登录

单点登录的思想很简单，即用户只需要登录应用一次，就可以访问应用中的所有微服务。这种解决方案意味着每个面向用户的服务都必须与身份验证服务进行交互，如下图所示：

使用SSO时，会导致大量非常琐碎的网络流量、重复工作，并可能导致单点故障。当有几十个微应用程序时，这种解决方案的缺点将变得更加明显。

#### 客户端Token与网关

用户的身份验证过程与Token身份验证的基本过程类似。不同之处在于API网关被添加为外部请求的入口。这种情况意味着所有请求都通过API网关，有效地隐藏了微服务。根据请求，API网关将原始用户令牌转换为只有自身才能解析的不透明令牌，如下图所示：

![token_and_gateway_flow](https://gitee.com/tobing/imagebed/raw/master/token_and_gateway_flow.png)

在这种情况下，注销不是一个问题，因为API网关可以在用户的令牌注销时撤销它，并且它还通过向客户机隐藏来为Auth令牌添加额外的保护，以防止其被解密。

#### 第三方应用访问

**API Token**

第三方使用应用程序发布的API令牌访问应用程序的数据。令牌由应用程序中的用户生成，并提供给第三方应用程序使用。在这种情况下，通常只允许第三方应用程序访问用户自己的令牌数据，而不允许访问其他用户的敏感私有数据。

例如，Github提供了个人API令牌函数。用户可以在Github的开发者设置界面中创建一个令牌，然后使用该令牌访问githubapi。在创建令牌时，可以设置令牌可以访问用户的哪些数据，例如查看Repo信息、删除Repo、查看用户信息、更新用户信息等。

使用API令牌而不是直接使用用户名/密码来访问API的优点是减少了暴露用户密码的风险，并且可以随时收回令牌的权限，而不必更改密码。

**OAuth**

一些第三方应用需要访问不同用户的数据，或者整合多个用户的数据。您可以考虑使用 OAuth。使用OAuth，当第三方应用程序访问服务时，应用程序提示用户授权第三方应用程序使用相应的访问权限，并根据用户的权限生成访问令牌。

例如，在 Github 中，一些第三方应用程序，如 GitBook 或 Travis CI，是通过 OAuth 和 Github 集成的。 OAuth 针对不同的场景有不同的认证流程。典型的认证过程如下图所示：

![auth_flow](https://gitee.com/tobing/imagebed/raw/master/auth_flow.png)

OAuth方式相比于API Token，减少了Access Token被盗用的风险。

## Spring Security

Spring Security 是基于 Spring 框架，提供了一套Web应用安全性的完整解决方案。一般而言，Web应用的安全性包括 Authentication 和 Authorization，即“认证”和“授权”，而这也是Spring Security的重要核心。

Spring Security是Spring技术栈的组合部分，主要具有以下特征。

+ 和Spring无缝整合。
+ 全面的权限控制。
+ 专门为Web开发设计。
+ 重量级。

而同为安全框架的Shiro则有所不同，Shiro是Apache旗下的轻量级权限控制框架。Shiro主要具有以下特点：

+ 轻量级。Shiro主张把复杂的事情变得简单。针对性能有更高要求的互联网应用有更好表现。
+ 通用性好。不局限与Web，可以脱离Web环境使用，在Web环境下一些特定的需求需要手动编码定制。

在Spring Boot 出现之前，Shiro 是安全管理领域的绝对强者。但是出现了 Spring Boot，Spring Boot 针对Spring Security提供了自动化的配置方案，可以用更少的配置使用Spring Security。因此，一般而言：

+ 对于SSM架构，往往采用 Shiro框架；
+ 对于Spring Boot 或 Spring Cloud，往往使用Spring Security。

[Spring Security Reference](https://docs.spring.io/spring-security/site/docs/current/reference/html5/)

### 权限管理概念

权限系统中，有以下概念名词：

+ 主体：principal

  使用系统的用户或设备或从其他系统远程登录的用户等。

  简单而言，主体就是系统使用者。

+ 认证：authentication

  权限管理系统确认一个主体的身份，允许主体进入系统。

  简单而言，认证就是“主体”证明自己是谁。可以认为是登录操作。

+ 授权：authorization

  将操作系统的权利赋予主体，这样主体就具备了操作系统中特定功能的能力。

### Spring Security基本原理

Spring Security本质上是有个过滤器链。从启动时，可以获取一系列过滤器，并执行其中的方法。其中有三个比价重要的过滤器：

+ FilterSecurityInterceptor：方法级权限过滤器，基本位于过滤链最底部。
+ ExceptionTranslationFilter：异常过滤器，用于处理认证授权过程中抛出的异常。
+ UsernamePasswordAuthenticationFilter：对于login的POST请求进行拦截，校验表单中的用户名和密码。

### UserDetailsService接口

默认情况下，用户名和密码是由Spring Security定义生成。但是实际项目中，账号和密码需要从数据库中查询而来，这是需要通过UserDetailService来定义逻辑控制认证逻辑。

> UserDetailsService

```java
public interface UserDetailsService {
	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
```

UserDetailsService接口中的loadUserByUsername方法的返回值是一个UserDetails，该类是系统默认的用户“主体”。

> UserDetails

```java
public interface UserDetails extends Serializable {

	// 获取登录用户的所有权限
	Collection<? extends GrantedAuthority> getAuthorities();

	// 获取密码
	String getPassword();

	// 获取用户名
	String getUsername();

	// 判断账号是否过期
	boolean isAccountNonExpired();

	// 判断账号是否锁定
	boolean isAccountNonLocked();

	// 判断凭证/密码是否过期
	boolean isCredentialsNonExpired();

	// 表示当前用户是否可用
	boolean isEnabled();
}
```

一般而言，我们只需要使用UserDetails的实现类org.springframework.security.core.userdetails.User即可。

### PasswordEncoder接口

实际中，不会使用明文来储存密码。PasswordEncoder接口定义了对密码的编码规范。

> PasswordEncoder

```java
public interface PasswordEncoder {

	// 把参数通过特定的解析规则进行解析
	String encode(CharSequence rawPassword);

	// 校验用户传来的rawPassword与编码后提交的原始密码是否匹配encodedPassword
	boolean matches(CharSequence rawPassword, String encodedPassword);

	// 表示如果解析的密码能够再次进行解析且达到更安全的结果则返回true，否则false
	default boolean upgradeEncoding(String encodedPassword) {
		return false;
	}
}
```

Spring Security官方推荐PasswordEncoder的实现类BCryptPasswordEncoder作为密码解析器。

> BCryptPasswordEncoder简单测试

```java
@Test
public void testBCryptPasswordEncoder() {
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    // 对字符串使用BCryptPasswordEncoder编码
    String tobing = bCryptPasswordEncoder.encode("tobing");
    System.out.println(tobing);
    // 使用字符串进行匹配
    boolean res = bCryptPasswordEncoder.matches("tobing", tobing);
    System.out.println(res);
}
```



## 微服务Web权限方案

### 微服务简介

微服务环境下，每个服务可以独立部署、运行、升级，服务与服务之间”松耦合“，而在功能上表现为一个统一整体。

### 微服务下的认证与授权

![image-20210726151015821](https://gitee.com/tobing/imagebed/raw/master/image-20210726151015821.png)

微服务系统中，往往模块众多，为了能够高效地对这些众多的服务进行授权与认证，往往会采用基于token的形式。

1. 首先，用户输入用户名密码进行身份认证，随后获取当前用户角色具有的一系列权限信息
2. 该用户的权限信息与用户名，会以：userId={auths}的形式保存到Redis中
3. 授权中心根据用户名相关信息，生成token返回
4. 以后客户端对服务进行访问时，会携带token（位于header中）
5. Spring Security会解析请求中的token信息，获取当前的userId，进而可以从Redis中获取到相应的权限列表
6. 这样一来，Spring Security可以判断当前请求是否有权限访问某个接口

## 微服务鉴权实战

### 简介

#### 微服务的无状态性

在微服务集群中的每个服务，都对外提供Rest风格的接口。而Rest接口具有一个重要的规范，就是服务的无状态性，即：

+ 客户端不保存任何客户端请求者信息
+ 客户端的每次请求必须具备自描述性，就可以通过这些自描述性信息识别客户端的身份

采用服务的无状态性，具有以下好处：

+ 客户端请求不依赖服务端的信息，任何请求不需要必须访问同一台服务器

+ 服务端的集群和状态对客户端透明
+ 服务端可以任意的迁移和伸缩
+ 减少服务端储存压力

#### 无状态登录流程

在使用无状态登录时，其具体的流程如下：

+ 客户端第一次请求服务时，服务端对用户进行信息认证（登录）
+ 认证通过，将用户信息进行加密形成token，返回给客户端，作为登录凭证
+ 以后每次请求，客户端都携带认证token
+ 服务端对token进行解密，判断是否有效

![1527300483893](https://gitee.com/tobing/imagebed/raw/master/1527300483893.png)

在整个登录过程中，由于token起到关键性的作用，是识别客户端身份的唯一标识，必须要保证不能被用伪造。因此需要采用JWT+RSA非对称加密技术。

JWT的签名部分，需要使用到服务端的密钥，密钥由服务端保证唯一性和安全性，集合RSA非对称算法，保证签名部分只能由服务器端生成，保证了token的可行度。

使用JWT认证时，流程如下：

- 1、用户登录
- 2、服务的认证，通过后根据secret生成token
- 3、将生成的token返回给浏览器
- 4、用户每次请求携带token
- 5、服务端利用公钥解读jwt签名，判断签名有效后，从Payload中获取用户信息
- 6、处理请求，返回响应结果

### Spring Security集合JWT实现权限管理

#### 1、创建认证模块

微服务架构下，认证授权的功能往往会抽取称为一个单独的服务：「认证服务」，所有需要认证授权的请求都需要需要经过「认证服务」获取Token，再通过Token来访问其他服务。

在「认证服务」中，需要引入关键的依赖：

```xml
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.9.0</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

#### 2、定制化Spring Security配置

使用Spring Security中关键一环就是需要定制化我们的Spring Security，这里主要需要配置以下信息：

+ 关闭CSRF：基于Token无需启用CSRF，无CSRF风险
+ 配置注销登录信息：包含注销登录的URL、注销登录的处理器，在处理器中主要是清除Token信息
+ 配置认证Filter：在认证Filter中主要是对用户的用户名和密码获取、认证成功回调、认证失败回调
+ 配置授权Filter：在授权Filter中主要是获取登录用户的用户名，并根据用户查询出该用户的权限信息，保存到Redis中
+ 配置认证业务逻辑：配置如何认证用户传递过来的用户和密码
+ 配置密码编码器：密码会进行加密存储，可以通过制定密码编码器来制定密码加密方式

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    // token模式下关闭csrf
    http.csrf().disable()
        // 指定认证异常处理类
        .exceptionHandling().authenticationEntryPoint(new TokenAuthenticationEntryPoint())
        .and()
        .authorizeRequests()
        .anyRequest().authenticated()
        .and()
        // 指定注销url地址
        .logout().logoutUrl(HandAuthConstant.AUTH_LOGOUT_URL)
        // 指定注销处理器
        .addLogoutHandler(new TokenLogoutHandler())
        .and()
        // 指定登录Filter
        .addFilter(new TokenUsernamePasswordAuthFilter(authenticationManager(), tokenManager, redisService))
        // 指定授权Filter
        .addFilter(new TokenBasicAuthFilter(authenticationManager(), tokenManager, redisService))
        .httpBasic();
}

@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    // 指定认证处理接口
    auth.userDetailsService(userDetailsService)
        // 指定密码编码器
        .passwordEncoder(passwordEncoder);
}

@Bean
public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
}
```

#### 3、编写认证异常处理

>认证失败时，主要需要是写回统一的错误状态码信息。

```java
public class TokenAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, 
                         HttpServletResponse response, 
                         AuthenticationException authException) throws IOException, ServletException {
        ResponseUtil.out(response, Result.error(BziCodeEnum.UNAUTH_ERROR.getCode(), 
                                                BziCodeEnum.UNAUTH_ERROR.getMsg()));
    }
}
```

#### 4、编写认证Filter

> Spring Security的两大核心功能是认证和授权。其中认证时判断用户为该系统合法用户，主要通过用户名和密码来识别。
>
> 在编写认证Filter时，需要继承UsernamePasswordAuthenticationFilter类，并重写其中的方法
>
> + attemptAuthentication：认证的具体过程，主要是获取用户请求中的用户名和密码，交给UserDetailsService处理
> + successfulAuthentication：认证成功回调，认证成功之后，只有需要执行两步，生成Token返回给客户端；将用户权限信息与用户名绑定存放到Redis中
> + unsuccessfulAuthentication：认证失败回调，返回认证失败的统一状态码

```java
public class TokenUsernamePasswordAuthFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenUsernamePasswordAuthFilter.class);

    private AuthenticationManager authenticationManager;
    private TokenManager tokenManager;
    private RedisService redisService;

    public TokenUsernamePasswordAuthFilter(AuthenticationManager authenticationManager,
                                           TokenManager tokenManager,
                                           RedisService redisService) {
        this.authenticationManager = authenticationManager;
        this.tokenManager = tokenManager;
        this.redisService = redisService;
        this.setPostOnly(false);
        // 设置登录接口地址以及登录请求方式
        this.setRequiresAuthenticationRequestMatcher(
                new AntPathRequestMatcher(HandAuthConstant.AUTH_LOGIN_URL, HandAuthConstant.AUTH_LOGIN_METHOD_TYPE));
    }

    /**
     * 认证过程
     * 从请求中获取用户填写的username、password封装为UserDTO，传递给UserDetailsService处理
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            UserDTO userDTO = new ObjectMapper().readValue(request.getInputStream(), UserDTO.class);
            return authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(userDTO.getUsername(), userDTO.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            LOGGER.warn("用户认证出现异常：{}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 认证成功回调函数
     * 1、根据认证成功的用户号，生成token返回前端
     * 2、以用户名为键，用户权限信息为值，将用户的权限信息保存到redis中
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        SecurityUser securityUser = (SecurityUser) authResult.getPrincipal();
        // 保存权限信息到Redis
        List<String> authList = null;
        if (!CollectionUtils.isEmpty(securityUser.getAuthorities())) {
            authList = securityUser.getAuthorities().stream()
                    // 过滤null值
                    .filter(item -> !StringUtils.isEmpty(item))
                    // 提取权限信息
                    .map(GrantedAuthority::getAuthority)
                    // 转换为List
                    .collect(Collectors.toList());
        }
        redisService.set(HandAuthConstant.AUTH_REDIS_PREFIX, securityUser.getUsername(), authList);
        // 返回token信息
        String token = tokenManager.createToken(securityUser.getUsername());
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        ResponseUtil.out(response, Result.ok(map));
    }

    /**
     * 认证失败回调函数
     * 返回认证错误状态码
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        ResponseUtil.out(response, Result.error(BziCodeEnum.UNAUTH_ERROR.getCode(), BziCodeEnum.UNAUTH_ERROR.getMsg()));
    }
}
```

#### 5、编写具体的认证业务逻辑

> 默认情况下，Spring Security会使用InMemoryUserDetailsManager来对用户传递过来的用户名和密码进行判断，这时是通过与内存中变量值作比较来校验用户合法性。但是实际中往往是需要到数据库中查询用户信息，因此这里实现UserDetailsService来覆盖原有实现。
>
> 在UserDetailsServiceImpl中主要是根据传递过来的用户名以及密码来判断合法性，以及查询用户的权限信息。

```java
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permissionService;

    /**
     * 根据username到数据查询用户数据，封装用户权限信息
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户权限信息
        UserDO userDO = userService.findOneByUsername(username);
        if (userDO == null) {
            throw new UsernameNotFoundException("用户号不存在");
        }
        // 获取授权列表
        List<PermissionDO> permissionDOList = permissionService.listPermissionValueByUserId(userDO.getId());
        // 封装用户授权信息
        SecurityUser securityUser = new SecurityUser();
        securityUser.setUserDO(userDO);
        if (!CollectionUtils.isEmpty(permissionDOList)) {
            Set<String> permissionListValues = permissionDOList
                    // 去除null元素
                    .stream().filter(Objects::nonNull)
                    // 提取权限信息
                    .map(PermissionDO::getPermissionValue)
                    // 组装为Set
                    .collect(Collectors.toSet());
            securityUser.setPermissionList(permissionListValues);
        }
        return securityUser;
    }
}
```

## 参考文献

+ [Microservices Authentication and Authorization Solutions](https://medium.com/tech-tajawal/microservice-authentication-and-authorization-solutions-e0e5e74b248a)
+ [尚硅谷SpringSecurity框架教程（spring security源码剖析从入门到精通）](https://www.bilibili.com/video/BV15a411A7kP)
+ [黑马-乐优商城](https://www.bilibili.com/video/BV11J41177it)
+ [黑马-十次方](https://www.bilibili.com/video/BV1a7411H7p9)

