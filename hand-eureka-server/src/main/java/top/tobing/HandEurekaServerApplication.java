package top.tobing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Author tobing
 * @Date 2021/7/19 20:50
 * @Description
 */
@EnableEurekaServer
@SpringBootApplication
public class HandEurekaServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(HandEurekaServerApplication.class, args);
    }
}
